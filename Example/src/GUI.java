/*     */ import java.awt.BorderLayout;
/*     */ import java.awt.Container;
/*     */ import java.io.IOException;
/*     */ import java.io.InputStream;
/*     */ import java.io.OutputStream;
/*     */ import java.io.PrintStream;
/*     */ import java.net.Socket;
/*     */ import javax.swing.JButton;
/*     */ import javax.swing.JFrame;
/*     */ 
/*     */ class GUI extends JFrame
/*     */ {
/*     */   ImagePanel imagePanel;
/*     */   JButton button;
/*  59 */   boolean firstCall = true;
/*     */   String server;
/*     */   int port;
/*  62 */   byte[] jpeg = new byte[131072];
/*     */ 
/* 139 */   private static final byte[] CRLF = { 13, 10 };
/*     */ 
/*     */   public GUI(String server, int port)
/*     */   {
/*  66 */     this.server = server;
/*  67 */     this.port = port;
/*  68 */     this.imagePanel = new ImagePanel();
/*  69 */     this.button = new JButton("Get image");
/*  70 */     this.button.addActionListener(new ButtonHandler(this));
/*  71 */     getContentPane().setLayout(new BorderLayout());
/*  72 */     getContentPane().add(this.imagePanel, "North");
/*  73 */     getContentPane().add(this.button, "South");
/*  74 */     setLocationRelativeTo(null);
/*  75 */     pack();
/*  76 */     refreshImage();
/*     */   }
/*     */ 
/*     */   public void refreshImage()
/*     */   {
/*     */     try {
/*  82 */       Socket sock = new Socket(this.server, this.port);
/*  83 */       InputStream is = sock.getInputStream();
/*  84 */       OutputStream os = sock.getOutputStream();
/*     */ 
/*  87 */       putLine(os, "GET /image.jpg HTTP/1.0");
/*  88 */       putLine(os, "");
/*     */ 
/*  92 */       String responseLine = getLine(is);
/*  93 */       System.out.println("HTTP server says '" + responseLine + "'.");
/*     */       do
/*     */       {
/*  96 */         responseLine = getLine(is);
/*     */       }
/*  95 */       while (!
/*  97 */         responseLine.equals(""));
/*     */ 
/* 100 */       int bufferSize = this.jpeg.length;
/* 101 */       int bytesRead = 0;
/* 102 */       int bytesLeft = bufferSize;
/*     */       int status;
/*     */       do {
/* 110 */         status = is.read(this.jpeg, bytesRead, bytesLeft);
/*     */ 
/* 113 */         if (status > 0) {
/* 114 */           bytesRead += status;
/* 115 */           bytesLeft -= status;
/*     */         }
/*     */       }
/* 117 */       while (status >= 0);
/* 118 */       sock.close();
/*     */ 
/* 120 */       System.out.println("Received image data (" + 
/* 121 */         bytesRead + " bytes).");
/*     */     }
/*     */     catch (IOException e)
/*     */     {
/* 125 */       System.out.println("Error when receiving image.");
/* 126 */       return;
/*     */     }
/*     */ 
/* 130 */     this.imagePanel.refresh(this.jpeg);
/* 131 */     if (this.firstCall) {
/* 132 */       pack();
/* 133 */       setVisible(true);
/* 134 */       this.firstCall = false;
/*     */     }
/*     */   }
/*     */ 
/*     */   private static String getLine(InputStream s)
/*     */     throws IOException
/*     */   {
/* 146 */     boolean done = false;
/* 147 */     String result = "";
/*     */ 
/* 149 */     while (!done) {
/* 150 */       int ch = s.read();
/* 151 */       if ((ch <= 0) || (ch == 10))
/*     */       {
/* 154 */         done = true;
/*     */       }
/* 156 */       else if (ch >= 32) {
/* 157 */         result = result + (char)ch;
/*     */       }
/*     */     }
/*     */ 
/* 161 */     return result;
/*     */   }
/*     */ 
/*     */   private static void putLine(OutputStream s, String str)
/*     */     throws IOException
/*     */   {
/* 169 */     s.write(str.getBytes());
/* 170 */     s.write(CRLF);
/*     */   }
/*     */ }
