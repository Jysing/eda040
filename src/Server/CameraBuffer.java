package Server;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import se.lth.cs.cameraproxy.MotionDetector;

/**
 * Class that contains a buffer of pictures.
 * Also contains system modes and sends out pictures in speed depending on it.
 * Whole class is a monitor.
 * 
 */
public class CameraBuffer {
	private Queue<byte[]> queue;
	private MotionDetector motionDetector;
	private byte mode;
	private final byte IDLE = 0, FORCED_IDLE = 1, MOVIE = 2, DC = 3;
	private long lastTimeStamp;
	private boolean connection;

	/**
	 * Construct the monitor class
	 * @param detector the proxycameras motiondetector
	 */
	CameraBuffer(MotionDetector detector) {
		queue = new LinkedList<byte[]>();
		mode = MOVIE;
		lastTimeStamp = 0;
		motionDetector = detector;
		connection = false;		

	}

	/**
	 * puts new image to buffer
	 * @param image new image in byte array
	 */
	synchronized void putImage(byte[] image) {
		image[0] = mode;
		if (mode == IDLE && motionDetector.detect()) {
			mode = MOVIE;
			image[0] = MOVIE;
		}

		if (mode == MOVIE || ((mode == IDLE || mode == FORCED_IDLE) && (System
						.currentTimeMillis() - lastTimeStamp) > 5000)) {
			lastTimeStamp = System.currentTimeMillis();
			if(queue.size() > 100){
				queue.poll();
			}
			queue.add(image);
		}

		notifyAll();
	}

	/**
	 * Returns the next available image and removes it from buffer
	 * Blocking method if buffer is empty.
	 * 
	 * @return next available image
	 */
	synchronized byte[] getNextImage() {
		while (queue.isEmpty())
			try{
				wait();
			}catch(InterruptedException e){
				
			}
		notifyAll();
		return queue.poll();
	}

	/**
	 * put camera mode
	 * 
	 * @param mode new mode
	 */
	synchronized void putMode(byte mode) {
		this.mode = mode;
		if(mode == DC){
			this.setConnection(false);
			notifyAll();
		}
	}

	/**
	 * Gets the connections status
	 * @return connection status
	 */
	synchronized boolean isConnected() {
		return connection;
	}

	/**
	 * Waits until someone calls setConnection(true)
	 * (Blocking)
	 * 
	 * @throws InterruptedException
	 */
	synchronized void waitForConnection() {
		while (!connection) {
			notifyAll();
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		notifyAll();
	}

	/**
	 * Signal connection status to the buffer.
	 * 
	 * @param c new connections status
	 */
	synchronized void setConnection(boolean c) {
		if(c) {
			System.out.println("Server should be connected.");
		} else {
			System.out.println("Server might be disconnected.");
		}
		connection = c;
		notifyAll();
	}

	/**
	 * Returns next image without removing it from buffer.
	 * 
	 * @return nextImage
	 */
	synchronized byte[] peekNextImage() {
		while(queue.isEmpty()){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		byte[] im = new byte[100000];
		im = Arrays.copyOfRange(queue.peek(), 1, 100001);
		return im;
		
	}

	/**
	 * blocking method which waits until server is disconnected.
	 */
	public synchronized void awaitDisconnect() {
		notifyAll();
		while(connection){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}

}
