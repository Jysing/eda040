 import java.awt.Image;
 import java.awt.Toolkit;
import javax.swing.ImageIcon;
 import javax.swing.JLabel;
import javax.swing.JPanel;

 class ImagePanel extends JPanel
 {
   ImageIcon icon;
 
  public ImagePanel()
   {
     this.icon = new ImageIcon();
    JLabel label = new JLabel(this.icon);
     add(label, "Center");
     setSize(200, 200);
  }

  public void refresh(byte[] data) {
    Image theImage = getToolkit().createImage(data);
     getToolkit().prepareImage(theImage, -1, -1, null);
     this.icon.setImage(theImage);
     this.icon.paintIcon(this, getGraphics(), 5, 5);
   }
 }
