package Client;

import java.util.*;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Buffer class which contains data from all cameras and their modes.
 * 
 * @author Tim & Julian
 * 
 */
public class ImageBuffer {
	private PriorityBlockingQueue<Image> prioQueue;
	private byte systemMode;
	final byte IDLE = 0, FORCED_IDLE = 1, MOVIE = 2, DC = 3;
	final int TO_IDLE = 2;
	private int nbrOfCameras;
	private int nbrOfDC = 0;
	private int trigger;
	private ArrayList<ConnectionAgent> connectionAgents;
	private boolean changeTrigger;
	private boolean syncMode = true;

	/**
	 * constructor
	 */
	ImageBuffer() {
		prioQueue = new PriorityBlockingQueue<Image>();
		systemMode = IDLE;
		trigger = -1;
		changeTrigger = true;
		connectionAgents = new ArrayList<ConnectionAgent>();
	}

	/**
	 * Get image from a specified camera
	 * 
	 * @return Image
	 */
	synchronized Image getNextImage() {
		while (prioQueue.isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		notifyAll();
		return prioQueue.poll();
	}

	/**
	 * 
	 * @return number of cameras in buffer
	 */
	synchronized int nbrOfCameras() {
		return nbrOfCameras;
	}

	/**
	 * puts an image in the buffer specified from the camIndex
	 * 
	 * @param image
	 * @param camIndex
	 */
	synchronized void putImage(byte[] image, int camIndex) {
		byte camMode = image[0];
		if (systemMode == IDLE && camMode == MOVIE) {
			systemMode = MOVIE;
			if (changeTrigger) {
				trigger = camIndex;
				changeTrigger = false;
			}
		}

		byte[] actualImage = Arrays.copyOfRange(image, 1, image.length);
		long timestamp = constructTimestamp(actualImage);
		if (prioQueue.size() > 20) {
			prioQueue.poll();
		}
		prioQueue.add(new Image(actualImage, timestamp, camIndex));

		notifyAll();
	}

	synchronized int getTrigger() {
		int tmp;
		if (systemMode == IDLE || systemMode == FORCED_IDLE) {
			return TO_IDLE;
		} else {
			tmp = trigger;
			trigger = -1;
			changeTrigger = true;
		}
		return tmp;
	}

	/**
	 * Finds the timestamp in an image (byte-array)
	 * 
	 * @param image
	 * @return timestamp
	 */
	private long constructTimestamp(byte[] image) {
		return (((image[25] & 255L) << 24 | (image[26] & 255L) << 16
				| (image[27] & 255L) << 8 | image[28] & 255L) * 1000 + (image[29] & 255L) * 10);
	}

	/**
	 * Simple data-container-class with imagedata and timestamp
	 * 
	 * @author Tim
	 * 
	 */
	class Image implements Comparable<Image> {
		private byte[] imageData;
		private long timestamp;
		private int cameraIndex;
		private long delay;

		private Image(byte[] actualImage, long timestamp, int cameraIndex) {
			this.imageData = actualImage;
			this.timestamp = timestamp;
			this.cameraIndex = cameraIndex;
			this.delay = 0;
		}

		byte[] getImageData() {
			return imageData;
		}

		long getTimestamp() {
			return timestamp;
		}

		int getCamerIndex() {
			return cameraIndex;
		}

		long getDelay() {
			return delay;
		}

		@Override
		public int compareTo(Image other) {
			return (int) (this.timestamp - other.timestamp);
		}

		public void setDelay(long delay2) {
			this.delay = delay2;
		}
	}

	/**
	 * Puts a new mode
	 * 
	 * @param mode
	 */
	synchronized void putMode(byte mode) {
		if (nbrOfCameras != 0)
			this.systemMode = mode;
		notifyAll();

	}

	/**
	 * Waits for a mode-change to happen and then returns the new one
	 * 
	 * @return newMode
	 */
	synchronized byte awaitModeChange() {
		notifyAll();
		byte oldMode = systemMode;
		while (oldMode == systemMode) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		return systemMode;
	}

	/**
	 * Gets the current mode
	 * 
	 * @return mode
	 */
	synchronized byte getMode() {
		return systemMode;
	}

	/**
	 * Returns a new integer for each new camera This is automatically called by
	 * CamReceiverThread when it is created.
	 * 
	 * @return camIndex
	 */
	synchronized int newCamera() {
		return ++nbrOfCameras;
	}

	/**
	 * method used by modeSenderThread to notify buffer that it has sent
	 * disconnect to server
	 */
	synchronized void notifyDC() {
		nbrOfDC++;
		if (nbrOfDC == nbrOfCameras) {
			putMode(MOVIE);
			nbrOfDC = 0;
			nbrOfCameras = 0;
			while(!connectionAgents.isEmpty()){
				connectionAgents.get(0).killThreads();
				connectionAgents.remove(0);
			}
		}
	}

	/**
	 * 
	 * @param connectionAgent
	 */
	void putCA(ConnectionAgent connectionAgent) {
		connectionAgents.add(connectionAgent);

	}

	void putSync(boolean b) {
		syncMode = b;
	}

	synchronized boolean getSyncMode() {
		return syncMode;
	}

}
