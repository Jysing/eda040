package Server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 
 * Simple HTTP server. Returns picture from camera if get request is sent.
 * Accessed by 127.0.0.1:8000 if on the same computer as server
 *
 */
public class HTTPServer extends Thread {

	private CameraBuffer buffer;
	private static final byte[] CRLF = { 13, 10 };

	HTTPServer(CameraBuffer buffer) {
		this.buffer = buffer;
	}

	@Override
	public void run() {
		super.run();
		try {
			handleRequests();
		} catch (IOException e) {
			System.out.println("HTTP server already running on server, won't start another one");
		}

	}

	/**
	 * Handles http request. Answers a get request with a picture
	 * Running on port 8000
	 * 
	 * @throws IOException
	 */
	public void handleRequests() throws IOException {
		ServerSocket serverSocket = new ServerSocket(0);
		System.out.println("HTTP server operating at port " + serverSocket.getLocalPort());

		while (!isInterrupted()) {
			try {
				// The 'accept' method waits for a client to connect, then
				// returns a socket connected to that client.
				Socket clientSocket = serverSocket.accept();

				// The socket is bi-directional. It has an input stream to read
				// from and an output stream to write to. The InputStream can
				// be read from using read(...) and the OutputStream can be
				// written to using write(...). However, we use our own
				// getLine/putLine methods below.
				InputStream is = clientSocket.getInputStream();
				OutputStream os = clientSocket.getOutputStream();

				// Read the request
				String request = getLine(is);

				// The request is followed by some additional header lines,
				// followed by a blank line. Those header lines are ignored.
				String header;
				boolean cont;
				do {
					header = getLine(is);
					cont = !(header.equals(""));
				} while (cont);

				System.out.println("HTTP request '" + request + "' received.");

				// Interpret the request. Complain about everything but GET.
				// Ignore the file name.
				if (request.length() >=4 && request.substring(0, 4).equals("GET ")) {
					// Got a GET request. Respond with a JPEG image from the
					// camera. Tell the client not to cache the image
					putLine(os, "HTTP/1.0 200 OK");
					putLine(os, "Content-Type: image/jpeg");
					putLine(os, "Pragma: no-cache");
					putLine(os, "Cache-Control: no-cache");
					putLine(os, ""); // Means 'end of header'

					
					os.write(buffer.peekNextImage());
				} else {
					// Got some other request. Respond with an error message.
					putLine(os, "HTTP/1.0 501 Method not implemented");
					putLine(os, "Content-Type: text/plain");
					putLine(os, "");
					putLine(os, "No can do. Request '" + request
							+ "' not understood.");

					System.out.println("Unsupported HTTP request!");
				}

				os.flush(); // Flush any remaining content
				clientSocket.close(); // Disconnect from the client
			} catch (IOException e) {
				System.out.println("Caught exception " + e);
			}
		}
		serverSocket.close();
	}

	/**
	 * Read a line from InputStream 's', terminated by CRLF. The CRLF is not
	 * included in the returned string.
	 */
	private static String getLine(InputStream s) throws IOException {
		boolean done = false;
		String result = "";

		while (!done) {
			int ch = s.read(); // Read
			if (ch <= 0 || ch == 10) {
				// Something < 0 means end of data (closed socket)
				// ASCII 10 (line feed) means end of line
				done = true;
			} else if (ch >= ' ') {
				result += (char) ch;
			}
		}

		return result;
	}

	/**
	 * Send a line on OutputStream 's', terminated by CRLF. The CRLF should not
	 * be included in the string str.
	 */
	private static void putLine(OutputStream s, String str) throws IOException {
		s.write(str.getBytes());
		s.write(CRLF);
	}
}
