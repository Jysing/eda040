package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import se.lth.cs.cameraproxy.Axis211A;
import se.lth.cs.cameraproxy.MotionDetector;

/**
 * Startup class for server. Contains main method
 */
public class Server {

	/**
	 * Main method for server. should be run with arguments.
	 * args[0] = IP to proxycamera
	 * args[1] = proxyport
	 * args[2] = localport
	 * 
	 * If localport is left out the server will choose the first available
	 * 
	 * @param args
	 */
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		int localport = 0;
		String ip = null;
		int proxyport = 0;
		
		try{
			if(args.length == 3) {
				localport = Integer.parseInt(args[2]);
			} else if (args.length == 2) {
				localport = 0;
			} else {
				throw new IllegalArgumentException("Invalid number of arguments");
			}
			ip = args[0];
			proxyport = Integer.parseInt(args[1]);
			if(ip == null || ip.isEmpty()){
				throw new NumberFormatException();
			}
		}catch(NumberFormatException e){
			System.err.println("Bad arguments! IP, proxyport or localport inserted wrong");
			System.exit(1);
		} catch (IllegalArgumentException e) {
			System.err.println("Illegal number of arguments. Must provide <ip> <proxyport> <localport> OR <ip> <proxyport> for automatic local port");
			System.exit(1);
		}
		
		ServerSocket serverSocket = null;
		
		try {
			serverSocket = new ServerSocket(localport);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		MotionDetector md = new MotionDetector(ip, proxyport);
		Axis211A camera = new Axis211A(ip, proxyport);		
		
		CameraBuffer buffer = new CameraBuffer(md);

		CameraImageThread camImThread = new CameraImageThread(buffer, camera);
		camImThread.start();

		HTTPServer hs = new HTTPServer(buffer);
		hs.start();
		
		
		while (true) {
			Socket clientSocket = null;
			try {
				System.out.println(serverSocket.toString());
				clientSocket = serverSocket.accept();
				buffer.setConnection(true);
			} catch (IOException e) {
				System.out.println("Caught exception " + e.getMessage());
			}

			ImageSenderThread imSenThread = new ImageSenderThread(buffer,
					clientSocket);
			imSenThread.start();

			ModeReceiverThread moReThread = new ModeReceiverThread(buffer,
					clientSocket);
			moReThread.start();

			buffer.awaitDisconnect();
			System.out.println("Disconnected");
		}
	}
}
