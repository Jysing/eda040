package Client;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.BorderLayout;

import javax.swing.SpringLayout;

import java.awt.GridBagLayout;

import javax.swing.JRadioButton;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.UnknownHostException;

/**
 * GUI class with cameras mode buttons, connect/disconnect buttons and other information.
 * @author ama10bjo
 *
 */
public class ClientGUI {

	private JFrame frame;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private ArrayList<CameraView> cameraViews;
	private ArrayList<JLabel> delayLabels = new ArrayList<JLabel>();
	private JLabel syncstatus = new JLabel("Synchronous");
	private JLabel triggeringCamera = new JLabel(" ");
	private ConnectionAgent connectionAgent;
	private ImageBuffer buffer;
	private JRadioButton movie;
	private JRadioButton idleForced;
	private JRadioButton idle;

	/**
	 * Updates the view for camera with index cameraIndex with jpeg
	 * 
	 * @param jpeg the image byte array
	 * @param cameraIndex the index of the camera
	 */

	public void updateCamera(byte[] jpeg, int cameraIndex) {
		CameraView view = cameraViews.get(cameraIndex);
		view.setImage(jpeg);
		if (buffer.getMode() == buffer.DC) {
			delayLabels.get(0).setText("DC");
		}
	}

	/**
	 * Set if GUI should display sync or async mode in the top label.
	 * 
	 * @param sync boolean value to be set
	 */
	public void setSync(boolean sync) {
		if (sync) {
			syncstatus.setText("Synchronous");
			syncstatus.setBackground(Color.green);
		} else {
			syncstatus.setText("Asynchronous");
			syncstatus.setBackground(Color.red);
		}
	}

	/**
	 * Create the application.
	 */
	public ClientGUI(ConnectionAgent cA, ImageBuffer buffer) {
		connectionAgent = cA;
		this.buffer = buffer;
		initialize();
	}

	/**
	 * Initialize the contents of the frame, including the cameras, radio buttons and connect/disconnect buttons.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 730, 454);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);

		JPanel cameras = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, cameras, 10,
				SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, cameras, 10,
				SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, cameras, -121,
				SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, cameras, -10,
				SpringLayout.EAST, frame.getContentPane());
		frame.getContentPane().add(cameras);

		JPanel buttons = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, buttons, 6,
				SpringLayout.SOUTH, cameras);
		springLayout.putConstraint(SpringLayout.WEST, buttons, 0,
				SpringLayout.WEST, cameras);
		springLayout.putConstraint(SpringLayout.SOUTH, buttons, -10,
				SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, buttons, -10,
				SpringLayout.EAST, frame.getContentPane());
		cameras.setLayout(new BorderLayout(0, 0));

		
		JPanel textfield = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, cameras, 20,
				SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, cameras, 10,
				SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, cameras, -121,
				SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, cameras, -10,
				SpringLayout.EAST, frame.getContentPane());
		frame.getContentPane().add(textfield);
		
		syncstatus.setHorizontalAlignment(SwingConstants.CENTER);
		cameras.add(syncstatus, BorderLayout.NORTH);
		
		triggeringCamera.setHorizontalAlignment(SwingConstants.SOUTH_EAST);
		textfield.add(triggeringCamera, BorderLayout.SOUTH);
		

		CameraView camera1 = new CameraView();
		cameras.add(camera1, BorderLayout.WEST);
		CameraView camera2 = new CameraView();
		cameras.add(camera2, BorderLayout.EAST);
		frame.getContentPane().add(buttons);
		GridBagLayout gbl_buttons = new GridBagLayout();
		gbl_buttons.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_buttons.rowHeights = new int[] { 0, 0, 0, 0 };
		gbl_buttons.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 1.0, Double.MIN_VALUE };
		gbl_buttons.rowWeights = new double[] { 1.0, 1.0, 1.0, Double.MIN_VALUE };
		buttons.setLayout(gbl_buttons);

		cameraViews = new ArrayList<CameraView>();
		cameraViews.add(camera1);
		cameraViews.add(camera2);

		JButton dc1 = new JButton("Disconnect");
		dc1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				buffer.putMode((byte) 3);

			}

		});

		GridBagConstraints gbc_btnDC = new GridBagConstraints();
		gbc_btnDC.insets = new Insets(0, 0, 2, 5);
		gbc_btnDC.gridx = 10;
		gbc_btnDC.gridy = 1;
		buttons.add(dc1, gbc_btnDC);

		JPanel delayPanel = new JPanel();
		JLabel label1 = new JLabel();
		delayPanel.add(label1, BorderLayout.WEST);
		JLabel label2 = new JLabel();
		delayPanel.add(label2, BorderLayout.EAST);
		cameras.add(delayPanel, BorderLayout.SOUTH);

		delayLabels.add(label1);
		delayLabels.add(label2);

		idleForced = new JRadioButton("Idle (forced)");
		buttonGroup.add(idleForced);
		GridBagConstraints gbc_idleForced = new GridBagConstraints();
		gbc_idleForced.insets = new Insets(0, 0, 5, 5);
		gbc_idleForced.anchor = GridBagConstraints.WEST;
		gbc_idleForced.gridx = 0;
		gbc_idleForced.gridy = 0;
		buttons.add(idleForced, gbc_idleForced);
		idleForced.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				buffer.putMode((byte) 1);

			}
		});

		idle = new JRadioButton("Auto");
		buttonGroup.add(idle);
		GridBagConstraints gbc_idle = new GridBagConstraints();
		gbc_idle.anchor = GridBagConstraints.WEST;
		gbc_idle.insets = new Insets(0, 0, 5, 5);
		gbc_idle.gridx = 0;
		gbc_idle.gridy = 1;
		buttons.add(idle, gbc_idle);
		idle.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				buffer.putMode((byte) 0);

			}
		});

		JButton btnConnect = new JButton("Connect");
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String ip = JOptionPane.showInputDialog("IP", "127.0.0.1");

				if (ip != null) {
					try {
						Integer port = Integer.parseInt(JOptionPane
								.showInputDialog("Port", "63676"));
						connectionAgent.startNewCamera(ip, port);
					} catch (UnknownHostException e) {
						JOptionPane
								.showMessageDialog(
										null,
										"Unable to establish connection, unknown host",
										"Connection problem",
										JOptionPane.ERROR_MESSAGE);
					} catch (IOException e) {
						JOptionPane
								.showMessageDialog(null,
										"Unable to establish connection",
										"Connection problem",
										JOptionPane.ERROR_MESSAGE);
					} catch (NumberFormatException e) {
						// no port input. do nothing
						System.out.println("Invalid port number");
					}
				}
			}
		});

		GridBagConstraints gbc_btnConnect = new GridBagConstraints();
		gbc_btnConnect.insets = new Insets(0, 0, 5, 5);
		gbc_btnConnect.gridx = 5;
		gbc_btnConnect.gridy = 1;
		buttons.add(btnConnect, gbc_btnConnect);

		movie = new JRadioButton("Movie");
		buttonGroup.add(movie);
		GridBagConstraints gbc_movie = new GridBagConstraints();
		gbc_movie.insets = new Insets(0, 0, 0, 5);
		gbc_movie.anchor = GridBagConstraints.WEST;
		gbc_movie.gridx = 0;
		gbc_movie.gridy = 2;
		buttons.add(movie, gbc_movie);
		movie.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				buffer.putMode((byte) 2);

			}
		});
		JRadioButton async = new JRadioButton("Force async", false);
		JRadioButton sync = new JRadioButton("Sync", true);

		ButtonGroup syncButtonGroup = new ButtonGroup();
		syncButtonGroup.add(async);
		GridBagConstraints gbc_async = new GridBagConstraints();
		gbc_async.insets = new Insets(0, 0, 5, 5);
		gbc_async.anchor = GridBagConstraints.WEST;
		gbc_async.gridx = 12;
		gbc_async.gridy = 1;
		buttons.add(async, gbc_async);
		
		syncButtonGroup.add(sync);
		GridBagConstraints gbc_sync = new GridBagConstraints();
		gbc_sync.insets = new Insets(0, 0, 0, 5);
		gbc_sync.anchor = GridBagConstraints.WEST;
		gbc_sync.gridx = 12;
		gbc_sync.gridy = 2;
		buttons.add(sync, gbc_sync);
		
		async.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				buffer.putSync(false);
				setSync(false);

			}
		});

		sync.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				buffer.putSync(true);
			}
		});

		this.frame.setVisible(true);

	}

	/**
	 * Gui registers new mode and alters the radiobuttons
	 * @param mode the new mode
	 */
	public void putMode(byte mode) {
		switch (mode) {
		case 0: //mode == IDLE
			idle.setSelected(true);
			break;
		case 1: //mode == IDLE
			idleForced.setSelected(true);
			break;
		default: //mode == MOVIE
			movie.setSelected(true);
			break;
		}

	}

	/**
	 * Prints the delay in the label corresponding to the appropriate camera
	 * @param camerIndex the index of the camera 
	 * @param delay the delay value to be put
	 */
	public void putDelay(int camerIndex, long delay) {
		delayLabels.get(camerIndex).setText("Delay: " + Long.toString(delay));

	}

	public void motionDetectionIn(int cameraIndex) {
		if (cameraIndex == buffer.TO_IDLE) {
			triggeringCamera.setText(" ");
			triggeringCamera.setBackground(Color.green);			
		} else {
			triggeringCamera.setText("Camera " + (cameraIndex + 1) + " detected motion");
			triggeringCamera.setBackground(Color.green);			
		}

	}
}
