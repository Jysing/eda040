package Client;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

/**
 * Thread to receive pictures from server
 * 
 * @author Tim
 *
 */
public class CamRecieverThread extends Thread {
	private ImageBuffer buffer;
	private Socket socket;
	private InputStream input;
	private int index;

	public CamRecieverThread(ImageBuffer buffer, Socket socket) {
		index = buffer.newCamera() - 1; // index starts at 0
		this.buffer = buffer;
		this.socket = socket;
	}

	/**
	 * opens a connection with ImageSenderThread and receives images over TCP/IP
	 */
	@Override
	public void run() {
		super.run();

		try {
			input = socket.getInputStream();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		while (buffer.getMode()!=buffer.DC) {
			try {
				byte[] image = new byte[100001];
				
				int read = 0;
				int status = 0;
			    while(buffer.getMode()!=buffer.DC && image.length>read) {
			    	status = input.read(image,read,(image.length-read));
			    	if(status > 0)
			    		read +=status;
			    }
			    buffer.putImage(image, index);
				
			} catch (IOException e) {
			}
		}
	}

}
