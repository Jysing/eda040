package Client;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * Connection Agent is used to set up new camera
 * it is also used to disconnect
 */
public class ConnectionAgent {

	private ImageBuffer buffer;
	private ArrayList<Thread> threads;

	public ConnectionAgent(ImageBuffer buffer) {
		this.buffer = buffer;
		threads = new ArrayList<Thread>();
		buffer.putCA(this);
	}

	/**
	 * Starts upp a new Camera connection
	 * 
	 * @param address
	 * @param port
	 * @return operating socket
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public Socket startNewCamera(String address, int port) throws UnknownHostException, IOException {
		Socket s = new Socket(address, port);
		
		CamRecieverThread camReceiverThread = new CamRecieverThread(buffer, s);
		ModeSenderThread modeSenderThread = new ModeSenderThread(buffer, s);
		threads.add(camReceiverThread);
		threads.add(modeSenderThread);
		camReceiverThread.start();
		System.out.println("Receiver thread started.");
		modeSenderThread.start();
		System.out.println("Sender thread started.");
		System.out.println("Connection to " + address + ":" + port
				+ " is probably established.");
		return s;
	}
	
	/**
	 * Kills all connections
	 */
	@SuppressWarnings("deprecation")
	public void killThreads(){
		for(int i =0; i<threads.size(); i++){
			threads.get(i).stop();
			threads.remove(i);
			i--;
		}
	}

}
