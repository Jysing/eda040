package Server;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

/**
 * Class that waits for a new mode to be received from client.
 * Then tells the buffer that the mode is changed
 *
 */
public class ModeReceiverThread extends Thread {
	private CameraBuffer buffer;
	private Socket socket;
	private InputStream input;

	ModeReceiverThread(CameraBuffer buffer, Socket socket) {
		this.buffer = buffer;
		this.socket = socket;
	}

	/**
	 * Waits for connection, then waits for incoming modes.
	 * When a mode is received it is passed to the buffer
	 */
	@Override
	public void run() {
		super.run();
		buffer.waitForConnection();
		try {
			input = socket.getInputStream();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		while (buffer.isConnected()) {
			byte mode;
			try {
				mode = (byte) input.read();
				if (mode != -1) {
					buffer.putMode(mode);
				}
			} catch (IOException e) {
				//sleeeep....
			}
		}
	}

}
