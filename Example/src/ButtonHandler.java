/*    */ import java.awt.event.ActionEvent;
/*    */ import java.awt.event.ActionListener;
/*    */ 
/*    */ class ButtonHandler
/*    */   implements ActionListener
/*    */ {
/*    */   GUI gui;
/*    */ 
/*    */   public ButtonHandler(GUI gui)
/*    */   {
/* 47 */     this.gui = gui;
/*    */   }
/*    */ 
/*    */   public void actionPerformed(ActionEvent evt) {
/* 51 */     this.gui.refreshImage();
/*    */   }
/*    */ }
