package Client;

import Client.ImageBuffer.Image;

/**
 * Thread to push pictures from buffer to GUI
 * 
 */
public class GuiThread extends Thread {
	private ImageBuffer buffer;
	private ClientGUI gui;
	private Image latestImage;

	public GuiThread(ImageBuffer buffer, ClientGUI gui) {
		this.buffer = buffer;
		this.gui = gui;
	}

	@Override
	public void run() {
		super.run();
		while (!this.isInterrupted())
			work();
	}

	/**
	 * Does the actual work. Takes picture in buffer and pushes it to GUI
	 */
	private void work() {
		Image image = buffer.getNextImage();
		long sleepTime = 0;
		long delay = System.currentTimeMillis() - image.getTimestamp();
		image.setDelay(delay);
		if (buffer.getSyncMode() && latestImage != null && image != null) {

			sleepTime = image.getTimestamp() - latestImage.getTimestamp()
					- image.getDelay();
			if (image.getCamerIndex() != latestImage.getCamerIndex()) {
				// different camera, needs to check synchronization
				if (sleepTime > 200) {
					while (sleepTime > 200) {
						// Fast forward, no sync
						gui.setSync(false);
						sleepTime = sleepTime / 2;
					}
				} else {
					gui.setSync(true);
				}
			}
		}
		try {
			if (!(sleepTime > 1000 || sleepTime < 0)) {
				sleep(sleepTime);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		int trigger = buffer.getTrigger();
		if (trigger >= 0) {
			gui.motionDetectionIn(trigger);
		}

		gui.updateCamera(image.getImageData(), image.getCamerIndex());
		gui.putMode(buffer.getMode());
		gui.putDelay(image.getCamerIndex(), image.getDelay());
		latestImage = image;

	}

}
