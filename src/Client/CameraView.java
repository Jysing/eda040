package Client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * CameraView contains an image which can be updated with setImage();
 */
@SuppressWarnings("serial")
public class CameraView extends JPanel {

	private JLabel imageLabel;
	private ImageIcon icon;

	/**
	 * Initialize the view specifications
	 */
	public CameraView() {
		setSize(new Dimension(320, 240));
		setPreferredSize(new Dimension(320, 240));
		setMinimumSize(new Dimension(320, 240));
		setMaximumSize(new Dimension(320, 240));
		setLayout(new BorderLayout(0, 0));

		icon = new ImageIcon();
		imageLabel = new JLabel(icon);
		add(imageLabel, BorderLayout.CENTER);

	}

	/**
	 * Sets image
	 * 
	 * @param jpeg contains image data in byte array
	 */
	public void setImage(final byte[] jpeg) {
		final CameraView thisMomma = this;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Image theImage = getToolkit().createImage(jpeg);
				getToolkit().prepareImage(theImage, -1, -1, null);
				icon.setImage(theImage);
				icon.paintIcon(thisMomma, thisMomma.getGraphics(), 5, 5);

			}
		});
	}
}
