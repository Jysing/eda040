package Server;

import java.io.IOException;
import java.io.OutputStream;
import java.net.*;

/**
 * Thread to send images from the buffer to host.
 *
 */
public class ImageSenderThread extends Thread {
	private CameraBuffer camBuffer;
	private OutputStream output;
	private Socket clientSocket;

	public ImageSenderThread(CameraBuffer buffer, Socket clientSocket) {
		camBuffer = buffer;
		this.clientSocket = clientSocket;
	}

	/**
	 * waits for connection with CamReceiverThread to be established and sends
	 * images in camBuffer to CamReceiverThread as quick as possible
	 */
	@Override
	public void run() {
		super.run();
		try {
			output = clientSocket.getOutputStream();

		} catch (IOException e) {
			System.out.println("Caught exception " + e);
		}
		while (camBuffer.isConnected()) {
			try {
				byte[] b = camBuffer.getNextImage();
				output.write(b);
				output.flush();
			} catch (IOException e) {
				//can't find client, trying again
			}
		}


	}

}
