package Server;
import se.lth.cs.cameraproxy.Axis211A;

/**
 * Thread that fetch images from camera and puts them in buffer
 * 
 */
public class CameraImageThread extends Thread {
	private CameraBuffer camBuffer;
	private Axis211A axisCamera;
	private int size = Axis211A.IMAGE_BUFFER_SIZE;
	
	public CameraImageThread(CameraBuffer camBuffer, Axis211A camera){
		this.camBuffer = camBuffer;
		System.out.println("Creating proxycamera");
		axisCamera = camera;
		axisCamera.connect();
		System.out.println("Proxycamera created");
	}
	
	
	@Override
	public void run() {
		byte[] data;
		while(!isInterrupted()){
			data = new byte[size+1];
			axisCamera.getJPEG(data, 1);
			camBuffer.putImage(data);
		}
	}

}
