package Client;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Thread class to send mode to server
 */
public class ModeSenderThread extends Thread {
	private ImageBuffer buffer;
	private Socket socket;

	public ModeSenderThread(ImageBuffer buffer, Socket socket) {
		this.buffer = buffer;
		this.socket = socket;
	}

	@Override
	public void run() {
		super.run();
		OutputStream output;
		try {
			output = socket.getOutputStream();
			output.write(buffer.getMode());
			output.flush();

			while (!isInterrupted()) {
				byte newMode = buffer.awaitModeChange();
				output.write(newMode);
				output.flush();
				System.out.println("Mode sent");

				if (newMode == buffer.DC) {
					buffer.notifyDC();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
