package Client;


/**
 * startup class for Client, contains main method
 * @author Tim
 *
 */
public class Client {

	/**
	 * Main method. No arguments needed
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		ImageBuffer imageBuffer = new ImageBuffer();
		ConnectionAgent cA = new ConnectionAgent(imageBuffer);
		ClientGUI clientGUI = new ClientGUI(cA, imageBuffer);

		GuiThread guiThread = new GuiThread(imageBuffer, clientGUI);
		guiThread.start();
		
		
	}

}
